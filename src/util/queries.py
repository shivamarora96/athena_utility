query_cpd_check = """

select 

date(psp.date_cpd) as date, 
count(distinct psp.wbn) as count_cpd,
count(distinct case when psp.spred is NOT NULL then psp.wbn end) as count_predicted

from express_dwh_3m.package_s3_parquet_3m psp
where psp.ad >= cast((current_date - interval '1' day) as varchar)
and psp.date_cpd >= (current_date - interval '1' day)
group by 1 order by 1 desc

"""
