import boto3
import csv
import time


class AthenaUtil:
    S3_OutputLocation = 's3://aws-athena-query-results-190020191201-us-east-1/'

    def __init__(self):
        self._credentials = None

        sts_client = boto3.client('sts')
        assumed_role_object = sts_client.assume_role(RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
                                                     RoleSessionName="SessionRole")
        self._credentials = assumed_role_object['Credentials']

    def _get_col_array(self, col_dict):

        """
        Get Col data from athena queryset

        """
        res = []

        # array dict of cols
        input_data = col_dict['Data']

        i = 0

        while (i < len(input_data)):
            current_dict = input_data[i]
            current_col = list(current_dict.values())[0]
            res.append(current_col)
            i = i + 1

        return res

    def _get_current_tuple(self, col_data, main_data, k):
        """

        Get Current Row from athena result
        :param main_data:
        :param k:
        :return:
        """
        current_tuple_array = main_data[k + 1]['Data']

        i = 0
        data = {}
        while (i < len(current_tuple_array)):
            k = col_data[i]
            v = ''

            current_data_col = current_tuple_array[i]

            if len(current_data_col) is not 0:
                v = list(current_data_col.values())[0]
            data[k] = v
            i = i + 1
        return data

    def _get_queryid(self, response, logger=None):
        """"
            Get query Execution id from athena response

        """
        if response is None:
            print('Response is NULL -  inside self._get_queryid function')
            raise ValueError('Response is NULL')

        return response['QueryExecutionId']

    # get query status
    def _get_status(self, client, id, logger=None):
        response = client.get_query_execution(
            QueryExecutionId=id
        )

        if response is None:
            print('Response is NULL for get status')
            raise ValueError('Response is NULL for get status')

        data_state = response['QueryExecution']
        state = data_state['Status']['State']
        # reason = data_state['Status']['StateChangeReason']
        reason = ''
        print('state - ' + state)
        # print('state', state)
        return {'status': state, 'reason': reason if reason is not None else ''}

    # get query data from athena
    def get_data(self, query=None):
        try:

            if query is None:
                raise ValueError('Nil query is passed to the athena')

            print(query)

            credentials = self._credentials

            client = boto3.client('athena', region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
                                  aws_secret_access_key=credentials['SecretAccessKey'],
                                  aws_session_token=credentials['SessionToken'])

            # client = boto3.client('athena', region_name='us-east-1')

            response = client.start_query_execution(QueryString=query,
                                                    ResultConfiguration={
                                                        'OutputLocation': AthenaUtil.S3_OutputLocation
                                                    }
                                                    )

            query_id = self._get_queryid(response)

            print('\n\nQuerying Data Please Wait ...\n\nid:', query_id)

            while True:
                query_meta = self._get_status(client, query_id)
                status = query_meta.get('status')
                reason = query_meta.get('reason')

                print('current status - ' + status)
                print('Reason', reason)

                QUERY_STATE_QUEUED = 'QUEUED'
                QUERY_STATE_RUNNING = 'RUNNING'
                QUERY_STATE_SUCCEEDED = 'SUCCEEDED'
                QUERY_STATE_FAILED = 'FAILED'
                QUERY_STATE_CANCELLED = 'CANCELLED'

                if status == QUERY_STATE_SUCCEEDED:
                    print('\nS\n')
                    result = []
                    temp = client.get_query_results(QueryExecutionId=query_id)
                    col_data = self._get_col_array(temp['ResultSet']['Rows'][0])
                    for k in range(1, len(temp['ResultSet']['Rows'])):
                        current_tuple = self._get_current_tuple(col_data, temp['ResultSet']['Rows'], k - 1)
                        result.append(current_tuple)

                    return result

                if status == QUERY_STATE_FAILED or status == QUERY_STATE_CANCELLED:
                    print('\nF\n')
                    result = []
                    print('Reason : ', reason)
                    return result

                time.sleep(3)
            return []

        except Exception as e:
            print('\nE\n', str(e))
            return []

    def _get_file(self, query=None):
        if query is None:
            print('Nil query is passed to the athena')
            raise ValueError('Nil query is passed to the athena')

        credentials = self._credentials

        client = boto3.client('athena', region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken'])

        s3_file_loc = AthenaUtil.S3_OutputLocation + '{}'
        response = client.start_query_execution(QueryString=query,
                                                ResultConfiguration={
                                                    'OutputLocation': AthenaUtil.S3_OutputLocation
                                                }
                                                )

        query_id = self._get_queryid(response)

        file = query_id + '.csv'

        print('Querying Data Please Wait... id:{}'.format(query_id))
        print('Querying Data Please Wait... id:{}'.format(query_id))

        while True:
            query_meta = self._get_status(client, query_id)
            status = query_meta.get('status')
            reason = query_meta.get('reason')

            print('current status - ' + status)

            QUERY_STATE_QUEUED = 'QUEUED'
            QUERY_STATE_RUNNING = 'RUNNING'
            QUERY_STATE_SUCCEEDED = 'SUCCEEDED'
            QUERY_STATE_FAILED = 'FAILED'
            QUERY_STATE_CANCELLED = 'CANCELLED'

            if status == QUERY_STATE_SUCCEEDED:
                print('success')
                time.sleep(2)
                return s3_file_loc.format(file)

            if status == QUERY_STATE_FAILED or status == QUERY_STATE_CANCELLED:
                print('\nFailed\n')
                print('Query File Failed ! - {}'.format(reason))
                return None

            time.sleep(3)
        return None

    def _convert_file_path_to_meta_data(self, file_path):

        # # s3://aws-athena-query-results-190020191201-us-east-1/243ebdeb-53fb-43cd-8512-0aeae4e2e516.csv
        #
        # BUCKET = 'aws-athena-query-results-190020191201-us-east-1'
        # key = '243ebdeb-53fb-43cd-8512-0aeae4e2e516.csv'

        if file_path is None:
            print('File path is none')
            return {}
        file_path = file_path.split('//')[1]
        bucket = file_path.split('/')[0]

        remaining_path_key = file_path.split('/')[1:]
        rem_len = len(remaining_path_key)
        key = ''
        i = 0
        for x in remaining_path_key:
            key = key + x
            if i != (rem_len - 1):
                key = key + '/'
            i += 1

        return {'bucket': bucket, 'key': key}

    def get_athena_data_from_s3_file(self, query=None):
        if query is None:
            return None

        temp_s3_file = self._get_file(query=query)
        print('file->', temp_s3_file)
        s3_meta_data = self._convert_file_path_to_meta_data(temp_s3_file)
        print('s3_mata', s3_meta_data)
        bucket = s3_meta_data.get('bucket')
        key = s3_meta_data.get('key')

        file_obj = boto3.resource('s3').Object(bucket, key)
        response = file_obj.get()
        lines = response[u'Body'].read().decode('utf-8').split()

        for row in csv.DictReader(lines):
            yield row

    def upload_file(self, file, file_rename=None, bucket=None):

        if bucket is None:
            raise Exception('Bucket is not defined !! in upload file athena fn')

        if file is None:
            raise Exception('File is NUll while uploading to athena !!')

        if file_rename is None:
            raise Exception('Uploading file name should be defined !!')

        print('stating uploading of ' + file + ' to ' + bucket + ' as ' + file_rename)

        credentials = self._credentials

        s3 = boto3.client('s3', region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
                          aws_secret_access_key=credentials['SecretAccessKey'],
                          aws_session_token=credentials['SessionToken'])

        try:
            s3.upload_file(file, bucket, 'demo/{}'.format(file_rename))
            print('Uploaded Successfully')
            print('Uploaded Successfully\n')
        except Exception as e:
            print('File upload failed - ' + str(e) + '\n')
            print('File upload failed - ' + str(e))


"""

Usage : 

try:
    a = AthenaUtil()
    obj = a.get_athena_data_from_s3_file(
        query="select * from express_dwh_3m.package_s3_parquet_3m where ad>'2019-11-20' limit 1")
    for x in obj:
        print(x)
except Exception as e:
    print(e)


"""
